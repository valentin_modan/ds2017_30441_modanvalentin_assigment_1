package database;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flights", catalog = "flights_dab")
public class Flight implements java.io.Serializable{

	private Integer nrFlight;
	private String airPlaneType;
	private City departureCity;
	private City arrivalCity;
	private Date departureTime;
	private Date arrivalTime;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "nrflight", unique = true, nullable = false)
	public Integer getNrFlight() {
		return nrFlight;
	}
	public void setNrFlight(Integer nrFlight) {
		this.nrFlight = nrFlight;
	}
	
	@Column(name = "airplanetype", nullable = false)
	public String getAirPlaneType() {
		return airPlaneType;
	}
	public void setAirPlaneType(String airPlaneType) {
		this.airPlaneType = airPlaneType;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "departurecity")
	public City getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "arrivalcity")
	public City getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	
	@Column(name = "departuredatetime", nullable = false)
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	
	@Column(name = "arrivaldatetime", nullable = false)
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	

	@Override
	public String toString()
	{
		String a = "Flight is " + nrFlight+ " " + departureCity.getCityName()+ " ";
		a = a + departureTime.toString() + " " + arrivalCity.getCityName() + " " + arrivalTime.toString()+ "\n"; 
		
		a = a + "Flight number" + nrFlight;
	
		return a;
	}
	
}
