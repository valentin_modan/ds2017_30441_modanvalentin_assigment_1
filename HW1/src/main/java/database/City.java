package database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cities", catalog = "flights_dab")
public class City {
	private String cityName;
	private Float latitude;
	private Float longitude;
	
	public City() {
	}
	
	public City(String cityName, Float latitude, Float longitude) {
		super();
		this.cityName = cityName;
		this.latitude = latitude;
		this.longitude = longitude;
	}


	@Id
	@Column(name = "name", unique = true, nullable = false, length = 45)
	public String getCityName() {
		return cityName;
	}
	
	
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	@Column(name = "lat")
	public Float getLatitude() {
		return latitude;
	}
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	
	@Column(name = "lng")
	public Float getLongitude() {
		return this.longitude;
	}
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}
	
	
}
