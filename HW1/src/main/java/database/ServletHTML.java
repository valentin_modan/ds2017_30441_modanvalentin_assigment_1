package database;

import java.io.PrintWriter;

public abstract class ServletHTML {

	public static void deleteButtonView(PrintWriter out){
	
	}
	
	private static void commonLabel(PrintWriter out,String word)
	{
		out.print("<label><b>");
		out.print(word);
		out.print(":</b></label> <br>\r\n" + 
		 		"		         	<input type = \"text\" \" name = \""+word+"\">\r\n" + 
		 		"		         	<br>");
	}
	
	private static void commonLabelNumber(PrintWriter out,String word)
	{
		out.print("<label><b>");
		out.print(word);
		out.print(":</b></label> <br>\r\n" + 
		 		"		         	<input type = \"number\" \" name = \""+word+"\">\r\n" + 
		 		"		         	<br>");
	}
	
	public static void createButtionView(PrintWriter out){
		out.print("<form action = \"ServletAdmin\" method = \"GET\">");
		commonLabelNumber(out,"FlightId");
		commonLabel(out,"AirplaneType");
		commonLabel(out,"DepartureCity");
		commonLabel(out,"DepartureDateTime");
		commonLabel(out,"ArrivalCity");
		commonLabel(out,"ArrivalDataTime");
		
		out.print("<input name=\"createButton\" type=\"submit\" value=\"CreateFlight\">");
		out.print("<input name=\"editFlight\" type=\"submit\" value=\"EditFlight\">");
		out.print("<input name=\"deleteFlight\" type=\"submit\" value=\"DeleteFlight\">");
		 out.print("  </form>");
	}
	
}
