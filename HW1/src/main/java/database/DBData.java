package database;

import org.hibernate.Session;
import org.hibernate.Transaction;

import utils.HibernateUtil;

public abstract class DBData {


	public static void saveFlight(Flight flight) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		Transaction t = session.beginTransaction();
		session.save(flight);
		t.commit();
		
		System.out.println("Succesfully added new flight");
	}
	
	public static void updateFlight(Flight flight) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		Transaction t = session.beginTransaction();
		session.update(flight);
		t.commit();
	}
	
	public static void deleteFlight(Flight flight) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		Transaction t = session.beginTransaction();
		session.delete(flight);
		t.commit();
		
	}
}
