package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.hibernate.Session;

import database.Login;
import utils.HibernateUtil;

/**
 * Servlet implementation class Servlet1
 */


public class Servlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		{
			System.out.println(request.getParameter("first_name") );
			System.out.println(request.getParameter("password") );
	
			Session session = HibernateUtil.getSessionFactory().openSession();
			Login login = (Login)session.get(Login.class, request.getParameter("first_name"));
			
			System.out.println("Test123");
			if(login==null)
			{
				System.out.println("Login failed!");
				response.sendRedirect("/HW1/");
			}
			else
			if(login.getPassword().equals(request.getParameter("password")))
			{
				System.out.println("Login successfull!");
				
				Login.setCurrentUserName(login.getTypeOfUser());
				
				if(login.getTypeOfUser().equals("admin")) {
					System.out.println("User is admin");
					
					response.sendRedirect("ServletAdmin");
				}
				else
				{
					System.out.println("User is normal");
					response.sendRedirect("ServletUser");
				
				}
			}
			
			
		}
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
