package servlets;


/*    <servlet>
     <servlet-name>PrintTable</servlet-name>
     <servlet-class>PrintTable</servlet-class>
 </servlet>

 <servlet-mapping>
     <servlet-name>PrintTable</servlet-name>
     <url-pattern>/PrintTable</url-pattern>
 </servlet-mapping>

*/
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Date;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import database.Login;
import database.ServletHTML;
import database.City;
import database.DBData;
import database.Flight;
import database.HtmlUtils;
import utils.HibernateUtil;

public class ServletUser extends HttpServlet {

public void doGet(HttpServletRequest request, HttpServletResponse response)
   throws ServletException, IOException {

 printTable(request, response);
}

public void doPost(HttpServletRequest request, HttpServletResponse response)
   throws ServletException, IOException {

 printTable(request, response);

}





public void printTable(HttpServletRequest request,
   HttpServletResponse response) throws IOException {

	
	if (!Login.getCurrentUserName().equals("client")) {
		System.out.println("User not client but trying to access client");
		Login.setCurrentUserName("notlogged");
		response.sendRedirect("/HW1/");
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}
 response.setContentType("text/html");
 PrintWriter out = response.getWriter();

 HtmlUtils hu = new HtmlUtils();

 out.print(hu.createHtmlHeader("Print Table"));

 //vm
 //Session session = HibernateUtil.getSessionFactory().openSession();
 //Login login = (Login)session.get(Login.class, request.getParameter("first_name"));

 Session session = HibernateUtil.getSessionFactory().openSession();
 


 List l = session.createCriteria(Flight.class).list();
 System.out.println("Total Number Of Records : "+l.size());
 



 out.print("<br><br>");
 out.print(hu.getTableHead("center", 1));

 out.print(hu.getTH("center", "Nr flight"));
 out.print(hu.getTH("center", "Airplane Type"));
 out.print(hu.getTH("center", "Departure City"));
 out.print(hu.getTH("center", "Departure Date Time"));
 out.print(hu.getTH("center", "Arrival City"));
 out.print(hu.getTH("center", "Arrival Date Time"));

 Vector av = new Vector();

 for(int i=0;i<l.size();i++)
 {
	Flight a = new Flight();
	a = (Flight) l.get(i);
	//System.out.println("Test " +a.getArrivalCity().getCityName() + " " + a.getDepartureCity().getCityName());
	
	av.addElement(a.getNrFlight());
	av.addElement(a.getAirPlaneType());
	av.addElement(a.getDepartureCity().getCityName());
	av.addElement(a.getDepartureTime().toString());
	av.addElement(a.getArrivalCity().getCityName());
	av.addElement(a.getArrivalTime().toString());
 }
 

 out.print(hu.getTableContents("center", av, 6));
 out.print(hu.getHtmlFooter());

}
}
